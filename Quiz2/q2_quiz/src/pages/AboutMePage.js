import AboutMe from "../components/AboutMe";

import {Box,Paper} from '@mui/material';


function AboutMePage(){
    
    return(

        
        <Box
        sx={{
          display: 'flex',
          flexWrap: 'wrap',
          justifyContent : 'center'
          }}
      >
          <br/>
            <Paper style= {{ width:'50%',height:'100%',marginTop:'10%' ,textAlign:'center'}}  elevation={3}  >  
                <h3> ผู้จัดทำเว็บนี้</h3> 
                <AboutMe name="นางสาวเมธาพร สกุลกาญจน์"
                studentID = "6310210329"
                year = "2"
                major = "วิทยาการคอมพิวเตอร์" />
            </Paper>
      </Box>
    
    );
}


export default AboutMePage;