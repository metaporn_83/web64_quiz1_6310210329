import GradeResult from "../components/GradeResult";
import {useState} from "react";

import {Grid,Box,Typography,Paper,TextField,Button} from '@mui/material/';


function CalculatePage(){
    
const [score,setScore ]  = useState(" ");    
const [grade,setGrade ]  = useState(" ");  

    function GradeCalculate(){
        let s = parseInt(score);

        if (s <= 100 && s>= 80) {
            setGrade("A");
          } else if (s <= 79 && s >= 75) {
            setGrade("B+");
          } else if (s <= 74 && s >= 70) {
            setGrade("B");
        } else if (s <= 69 && s >= 65) {
            setGrade("C+");
        } else if (s <= 64 && s >= 60) {
            setGrade("C");
        } else if (s <= 59 && s >= 55) {
            setGrade("D+");
        } else if (s <= 59 && s >= 55) {
            setGrade("D+");
        } else {
            setGrade("E");
          }
       
    }

    return(
        <Box>
            
            <Typography variant="h3" marginTop={5} marginBottom={5}>Grade Calculator</Typography>  
             <Box>
             <Grid item xs={12} >
                    <Typography variant="h6" marginBottom={3}  fontWeight='bold' >
                        กรุณากรอกคะแนนในรายวิชา 344-200 ของคุณ
                    </Typography>
            </Grid>
                    <TextField 
                        TextField id="outlined-basic" 
                        label="Score" 
                        variant="outlined"
                        size = 'small'
                        onChange={ (e) => {setScore(e.target.value);} }/>
                    
                    &nbsp; &nbsp; &nbsp;

                    <Button onClick={()=> {GradeCalculate()} } variant="contained" color="primary" size='500'>
                        Calculate </Button>

                {  (grade != 0) &&
                        <GradeResult
                            result={grade}    
                        />
                }
            </Box>
        </Box>
    );
}

export default CalculatePage;