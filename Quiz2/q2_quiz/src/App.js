import logo from './logo.svg';
import './App.css';

import Header from './components/Header'
import AboutMePage from './pages/AboutMePage' ;
import CalculatePage from './pages/CalculatePage';

import { Routes, Route } from "react-router-dom";
import {ThemeProvider, createTheme } from '@mui/material/styles';

function App() {
  return (

  <div class="App">
        <Header/>
        <Routes>

        <Route path = "/" element = {
                  <CalculatePage />} />

        <Route path = "about" element ={
                  <AboutMePage /> }/>

       </Routes>

    </div>
  );
}

export default App;
