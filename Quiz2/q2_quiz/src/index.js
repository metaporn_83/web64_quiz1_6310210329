import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from "react-router-dom";
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';

import {ThemeProvider, createTheme } from '@mui/material/styles';



const theme = createTheme({
  palette: {
    type: 'dark',
    primary: {
      main: '#af4685',
    },
    secondary: {
      main: '#c387a0',
    },
    background: {
      default: '#1e1f27',
      paper: 'rgba(14,13,14,0.88)',
    },
    success: {
      main: '#3cb940',
    },
    text: {
      primary: '#fdfdfd',
    },
  }
  });

ReactDOM.render(
  <BrowserRouter>
    <ThemeProvider theme={theme}>
        <App />
    </ThemeProvider>
  </BrowserRouter>,
  document.getElementById('root')
);
// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
