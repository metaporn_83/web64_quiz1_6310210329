
function AboutMe(props) {
    return(
        <div >
            <h4>จัดทำโดย : {props.name}</h4>
            <h4>รหัสนักศึกษา :{props.studentID}</h4>
            <h4>ชั้นปีที่ : {props.year}</h4>
            <h4>สาขาวิชา : {props.major}</h4>
        </div>
    );

    }
export default AboutMe;