import {Link } from "react-router-dom";
import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import Stack from '@mui/material/Stack' ; 


function Header(){

    return(

        <Box sx={{ flexGrow: 1 }}>
        <AppBar position="static">
          <Toolbar>
            <Typography variant="h6" >Grade Calculator</Typography> 
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <Stack direction="row" spacing={2}>
                <Button component={Link} to="/" variant="contained" color="primary">
                        คำนวณเกรด </Button>
                    <Button component={Link} to="/about" variant="contained" color="primary">
                        ผู้จัดทำ </Button>
                </Stack>

          </Toolbar>
        </AppBar>
      </Box>
    );


}

export default Header;